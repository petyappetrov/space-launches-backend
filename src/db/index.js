/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import mongoose from 'mongoose'
import requireDir from 'require-dir'
import config from '../config'

const fixtures = requireDir('./fixtures')

/**
 * Load fixtures from json files
 */
const loadFixtures = () =>
  Promise.all(Object.keys(mongoose.models).map(async (modelName) => {
    const model = mongoose.model(modelName)
    const fixture = fixtures[modelName.toLocaleLowerCase()]
    const item = await model.findOne()
    if (!item) {
      return model.create(fixture)
    }
    return Promise.resolve()
  }))

/**
 * Connect to database from mongoose
 */
mongoose.connect(config.get('db').host, {
  useCreateIndex: true,
  dbName: config.get('db').name,
  useNewUrlParser: true
})

/**
 * Show error in console
 */
mongoose.connection.on('error', console.error)

/**
 * Hooks after open DB
 */
mongoose.connection.on('open', async () => {
  /**
   * Load all models
   */
  requireDir('./models')

  /**
   * Wait loading fixtures
   */
  await loadFixtures()

  /**
   * Run server
   */
  require('../server')
})
