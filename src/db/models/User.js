/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import mongoose from 'mongoose'
import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'
import { forEach } from 'lodash'
import Joi from 'joi'
import { UserInputError } from 'apollo-server'
import config from '../../config'

/**
 * User schema
 */
const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    trim: true,
    type: String,
    required: true,
    unique: true,
    validate: [
      (email) => Joi.validate(email, Joi.string().email()),
      'Неверная электронная почта'
    ]
  },
  password: String,
  photoUrl: String,
  about: String
}, {
  versionKey: false,
  timestamps: true
})

/**
 * Pre hooks
 */
UserSchema.pre('save', async function (next) {
  if (!this.isModified('password')) {
    return next()
  }
  try {
    const salt = await bcrypt.genSalt(10)
    const hash = await bcrypt.hash(this.password, salt)
    this.password = hash
    return next()
  } catch (error) {
    return next(error)
  }
})

/**
 * Static
 */
UserSchema.statics = {
  async authentication ({ email, password }) {
    const user = await this.findOne({ email })
    if (!user) {
      throw new UserInputError('Ошибка авторизации', {
        invalidArgs: {
          email: 'Пользователь с таким email адресом не зарегистрирован'
        }
      })
    }
    const isMatch = await bcrypt.compare(password, user.password)
    if (!isMatch) {
      throw new UserInputError('Ошибка авторизации', {
        invalidArgs: {
          password: 'Неверный пароль'
        }
      })
    }

    user.jwt = jwt.sign({ _id: user._id }, config.get('secret').jwt)

    return user
  },

  getPaginatedList ({ filters, sortField, sortOrder, limit, skip, search }) {
    const match = {}

    /**
     * Search by name
     */
    if (search) {
      match.name = {
        $regex: search,
        $options: 'i'
      }
    }

    /**
     * Filters
     */
    if (filters) {
      forEach(filters, (filters, field) => {
        if (filters.length) {
          match[field] = {
            $in: filters
          }
        }
      })
    }

    /**
     * Stages for aggregation
     */
    const stages = [
      {
        $match: match
      }
    ]

    /**
     * Sorting by fields
     */
    if (sortField && sortOrder) {
      stages.push({
        $sort: {
          [sortField]: sortOrder === 'ascend' ? 1 : -1
        }
      })
    }

    /**
     * By pagination
     */
    if (skip) {
      stages.push({
        $skip: skip
      })
    }
    if (limit) {
      stages.push({
        $limit: limit
      })
    }

    return {
      items: this.aggregate(stages),
      count: this.countDocuments()
    }
  }
}

/**
 * Create model
 */
mongoose.model('User', UserSchema)
