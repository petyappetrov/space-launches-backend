/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import request from 'request-promise-native'
import config from '../config'

class API {
  /**
   * To return the launch by id
   *
   * @param {string} id - The ID of the launch you are searching for
   * @return {Promise}
   */
  getLaunch ({ id }) {
    return request({
      method: 'GET',
      url: config.get('api') + '/launch',
      json: true,
      qs: {
        id
      }
    })
  }

  /**
   * To return the launches
   *
   * @param {number} limit - Limit of responses. Defaults to 9
   * @param {number} offset - Offset, for pagination
   * @param {string} startdate - Date/Time to start search for. If this an
   * @param {string} launchId - Date/Time for the next launch attempt
   * @param {string} sort - Sorts by NET. Use asc for ascending, desc for descending
   * @return {Promise}
   */
  getLaunches ({
    limit = 9,
    offset = 0,
    startdate,
    enddate,
    sort = 'asc'
  }) {
    return request({
      method: 'GET',
      url: config.get('api') + '/launch',
      json: true,
      qs: {
        limit,
        offset,
        startdate,
        enddate,
        sort
      }
    })
  }

  /**
   * To return the mission with a launch ID
   *
   * @param {string} launchId - launch ID
   * @return {Promise}
   */
  getMissions ({ launchid }) {
    return request({
      method: 'GET',
      url: config.get('api') + '/mission',
      json: true,
      qs: {
        launchid
      }
    })
  }

  /**
   * To return the agency by ID
   *
   * @param {string} id - The id of a specific agency you want
   * @return {Promise}
   */
  getAgency ({ id }) {
    return request({
      method: 'GET',
      url: config.get('api') + '/agency',
      json: true,
      qs: {
        id
      }
    })
  }

  /**
   * To return the agencies
   *
   * @param {number} limit - Limit of responses. Defaults to 10
   * @param {number} offset - Offset, for pagination
   * @param {string} sort - Sorts by NET. Use asc for ascending, desc for descending
   * @return {Promise}
   */
  getAgencies ({
    limit = 10,
    offset = 0,
    sort = 'asc'
  }) {
    return request({
      method: 'GET',
      url: config.get('api') + '/agency',
      json: true,
      qs: {
        limit,
        offset,
        sort
      }
    })
  }
}

export default new API()
