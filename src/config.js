/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import convict from 'convict'

/**
 * Parse .env file and include in process.env
 */
require('dotenv').config()

/**
 * Schema for env variables
 */
const config = convict({
  env: {
    env: 'NODE_ENV',
    doc: 'The application environment.',
    format: [
      'production',
      'development'
    ],
    default: 'development'
  },
  port: {
    env: 'PORT',
    doc: 'The port to bind.',
    format: 'port',
    default: 8080
  },
  db: {
    host: {
      doc: 'Database host name/IP',
      format: '*',
      default: 'mongodb://127.0.0.1:27017'
    },
    name: {
      doc: 'Database name',
      format: String,
      default: 'space-launches'
    }
  },
  api: {
    doc: 'API host name/IP',
    format: 'url',
    default: 'https://launchlibrary.net/1.4'
  },
  secret: {
    jwt: {
      doc: 'Secret text for JSON Web Token',
      format: String,
      default: 'b8fb39b4468a43f3b8aa732d06a687ec'
    }
  }
})

/**
 * Project configuration variables
 */
export default config
