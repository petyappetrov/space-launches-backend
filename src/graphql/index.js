/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import { merge } from 'lodash'

import UsersSchema from './users/schema.graphql'
import UsersResolvers from './users/resolver'

import LaunchesSchema from './launches/schema.graphql'
import LaunchesResolvers from './launches/resolver'

import AgenciesSchema from './agencies/schema.graphql'
import AgenciesResolvers from './agencies/resolver'

/**
 * Types schemas
 */
export const typeDefs = [
  UsersSchema,
  LaunchesSchema,
  AgenciesSchema
]

/**
 * Resolvers
 */
export const resolvers = merge(
  UsersResolvers,
  LaunchesResolvers,
  AgenciesResolvers
)
