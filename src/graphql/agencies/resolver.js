/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import { ApolloError } from 'apollo-server'
import API from '../../api'

/**
 * Agencies resolvers
 */
const AgenciesResolvers = {
  Query: {
    agency: (root, args) =>
      API.getAgency(args)
        .then(res => res.agencies[0])
        .catch(err => new ApolloError(err.message)),

    agencies: (root, args) =>
      API.getAgencies(args)
        .then(res => res.agencies)
        .catch(err => new ApolloError(err.message))
  },
  Mutation: {}
}

export default AgenciesResolvers
