/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import { ApolloError } from 'apollo-server'
import API from '../../api'

/**
 * Launches resolvers
 */
const LaunchesResolvers = {
  Query: {
    launch: (root, args) =>
      API.getLaunch(args)
        .then(res => res.launches[0])
        .catch(err => new ApolloError(err.message)),

    launches: (root, args) =>
      API.getLaunches(args)
        .then(res => res.launches)
        .catch(err => new ApolloError(err.message))
  },
  Launch: {
    missions: (root, args) =>
      API.getMissions({ launchid: root.id })
        .then(res => res.missions)
        .catch(err => new ApolloError(err.message))
  },
  Mutation: {}
}

export default LaunchesResolvers
