/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import mongoose from 'mongoose'
import moment from 'moment'
import { AuthenticationError, UserInputError, ForbiddenError } from 'apollo-server'
import { isEmpty } from 'lodash'

const User = mongoose.model('User')

/**
 * Users resolvers
 */
const UsersResolvers = {
  Query: {
    users: (root, args) => User.getPaginatedList(args),
    user: (root, args) => User.findById(args._id),
    me: (root, args, context) => {
      if (isEmpty(context.user)) {
        return null
      }
      return User.findById(context.user._id)
    }
  },
  User: {
    createdAt: (root, args) => moment(root.createdAt).format(args.format)
  },
  Mutation: {
    registerUser: async (root, args) => {
      const user = await User.findOne({ email: args.email })
      if (user) {
        throw new UserInputError('Ошибка регистрации', {
          invalidArgs: {
            email: 'Пользователь с таким e-mail уже зарегистрирован'
          }
        })
      }

      const a = await User.create(args)
      return a
    },
    loginUser: (root, args) => User.authentication(args),
    removeUser: (root, args) => User.findOneAndRemove({ _id: args._id }),
    updateUser: async (root, args, context) => {
      if (!context.user) {
        throw new AuthenticationError()
      }
      const user = await User.findByIdAndUpdate(context.user._id, args, { new: true })
      if (!user) {
        throw new ForbiddenError()
      }
      return user
    }
  }
}

export default UsersResolvers
