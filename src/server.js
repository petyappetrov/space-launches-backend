/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import { ApolloServer } from 'apollo-server'
import jwt from 'jsonwebtoken'
import { typeDefs, resolvers } from './graphql'
import config from './config'

/**
 * Getting token from authorization header
 */
const getToken = (req) => {
  const authorization = req.headers && req.headers.authorization
  if (!authorization) {
    return null
  }
  const parts = authorization.split(' ')
  if (parts.length === 2) {
    if (/^Bearer$/i.test(parts[0])) {
      return parts[1]
    }
  }
}

/**
 * Passing decoded JWT to context
 */
const context = ({ req }) => {
  const token = getToken(req)
  if (!token) {
    return null
  }
  return jwt.verify(token, config.get('secret').jwt, (error, user) => {
    if (error) {
      return null
    }
    return { user }
  })
}

/**
 * Initialize server
*/
const server = new ApolloServer({
  typeDefs,
  resolvers,
  context
})

/**
 * Start application
 */
server.listen(config.get('port')).then(({ url }) =>
  console.log(`✅  The server is running at ${url}`)
)
